@extends('homepage')

@section('content')

    <section class="" style="padding-top: 70px;">
        <div class="container jumbo-container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="panel panel-30">
                        <div class="panel-heading">
                            <h3 class="panel-title text-center" style="font-weight:bold;">{{language_data('Sign to your account')}}</h3>
                        </div>
                        <div class="panel-body">
                            <form class="" role="form" method="post" action="{{url('admin/get-login')}}">
                                <div class="form-group form-group-default required">
                                    <label for="user name">{{language_data('User Name')}}</label>
                                    <input type="text" class="form-control" required name="username">
                                </div>
                                <div class="form-group form-group-default required">
                                    <label for="password">{{language_data('Password')}}</label>
                                    <input type="password" class="form-control" required name="password">
                                </div>

                                @if(app_config('captcha_in_admin')=='1')
                                <div id="g-recaptcha" class="g-recaptcha" data-sitekey="{{app_config('captcha_site_key')}}" data-expired-callback="recaptchaCallback"></div>

                                <noscript>
                                    <div style="width: 302px; height: 352px;margin-bottom:20px;margin-left:100px;">
                                        <div style="width: 302px; height: 352px; position: relative;">
                                            <div style="width: 302px; height: 352px; position: absolute;">
                                                <!-- change YOUR_SITE_KEY with your google recaptcha key -->
                                                <iframe src="https://www.google.com/recaptcha/api/fallback?k={{app_config('captcha_site_key')}}" style="width: 302px; height:352px; border-style: none;">
                                                </iframe>
                                            </div>
                                            <div style="width: 250px; height: 80px; position: absolute; border-style: none; bottom: 21px; left: 25px; margin: 0px; padding: 0px; right: 25px;">
                                                <textarea id="g-recaptcha-response" name="g-recaptcha-response" class="g-recaptcha-response" style="width: 250px; height: 80px; border: 1px solid #c1c1c1; margin: 0px; padding: 0px; resize: none;"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </noscript>
                                @endif


                                <div class="form-group m-t-20 m-b-20">
                                    <div class="coder-checkbox">
                                        <input type="checkbox" checked name="remember">
                                        <span class="co-check-ui"></span>
                                        <label>{{language_data('Remember Me')}}</label>
                                    </div>
                                </div>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="submit" class="btn btn-primary btn-block btn-lg" value="{{language_data('Login')}}">
                            </form>
                            <br>
                            @if(app_config('AppStage') == 'Demo')
                                <div class="alert alert-info alert-dismissible  text-uppercase text-center text-danger" role="alert">
                                    Demo Resets Every 1 Hour
                                </div>
                            @endif
                            @include('notification.notify')
                        </div>
                    </div>
                    <div class="panel-other-acction">
                        <div class="text-md text-center">
                            <a href="{{url('admin/forgot-password')}}">{{language_data('Forget Password')}}?</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection


{{--External Style Section--}}
@section('style')
    {!! Html::script("assets/libs/chartjs/chart.js")!!}
@endsection
<div style="margin:0;padding:0">
    <table cellspacing="0" cellpadding="0" width="100%" border="0" bgcolor="#0095eb">
        <tbody>
            <tr>
                <td align="center">
                    <table cellspacing="0" cellpadding="0" width="672" border="0">
                        <tbody>
                            <tr>
                                <td height="95" bgcolor="#0095eb" style="background:#0095eb;text-align:left">
                                    <table cellspacing="0" cellpadding="0" width="672" border="0">
                                        <tbody>
                                            <tr>
                                                <td width="672" height="40" style="font-size:40px;line-height:40px;height:40px;text-align:left"></td>
                                            </tr>
                                            <tr>
                                                <td style="text-align:left">
                                                    <table cellspacing="0" cellpadding="0" width="672" border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td width="37" height="24" style="font-size:40px;line-height:40px;height:40px;text-align:left">
                                                                </td>
                                                                <td width="523" height="24" style="text-align:left">
                                                                    <p style="display:block;color:#ffffff;font-size:20px;font-family:Arial,Helvetica,sans-serif;max-width:557px;min-height:auto">{{$data['business_name']}} </p>
                                                                </td>
                                                                <td width="44" style="text-align:left"></td>
                                                                <td width="30" style="text-align:left"></td>
                                                                <td width="38" height="24" style="font-size:40px;line-height:40px;height:40px;text-align:left"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="672" height="33" style="font-size:33px;line-height:33px;height:33px;text-align:left"></td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>

    <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#0095eb">
        <tbody>
            <tr>
                <td height="5" style="background:#0095eb;height:5px;font-size:5px;line-height:5px"></td>
            </tr>
        </tbody>
    </table>

    <table cellspacing="0" cellpadding="0" width="100%" border="0" bgcolor="#e9eff0">
        <tbody>
            <tr>
                <td align="center">
                    <table cellspacing="0" cellpadding="0" width="671" border="0" bgcolor="#e9eff0" style="background:#e9eff0">
                        <tbody>
                            <tr>
                                <td width="38" height="30" style="font-size:30px;line-height:30px;height:30px;text-align:left"></td>
                                <td width="596" height="30" style="font-size:30px;line-height:30px;height:30px;text-align:left"></td>
                                <td width="37" height="30" style="font-size:30px;line-height:30px;height:30px;text-align:left"></td>
                            </tr>
                            <tr>
                                <td width="38" height="40" style="font-size:40px;line-height:40px;height:40px;text-align:left"></td>
                                <td style="text-align:left">
                                    <table cellspacing="0" cellpadding="0" width="596" border="0" bgcolor="#ffffff">
                                        <tbody>
                                            <tr>
                                                <td width="20" height="26" style="font-size:26px;line-height:26px;height:26px;text-align:left"></td>
                                                <td width="556" height="26" style="font-size:26px;line-height:26px;height:26px;text-align:left"></td>
                                                <td width="20" height="26" style="font-size:26px;line-height:26px;height:26px;text-align:left"></td>
                                            </tr>
                                            <tr>
                                                <td width="20" height="26" style="font-size:26px;line-height:26px;height:26px;text-align:left"></td>
                                                <td width="556" style="text-align:left">
                                                    <table cellspacing="0" cellpadding="0" width="556" border="0" style="font-family:helvetica,arial,sans-seif;color:#666666;font-size:16px;line-height:22px">
                                                        <tbody>
                                                            <tr>
                                                                <td style="text-align:left"></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="text-align:left">
                                                                    <table cellspacing="0" cellpadding="0" width="556" border="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style="font-family:helvetica,arial,sans-serif;font-size:30px;line-height:40px;font-weight:normal;color:#253c44;text-align:left"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td width="556" height="20" style="font-size:20px;line-height:20px;height:20px;text-align:left"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="text-align:left">
                                                                                    Hi {{$data['name']}},<br>
                                                                                    <br> Password Reset Successfully! This message is an automated reply to your password reset request. Click this link to reset your password:
                                                                                    <br>
                                                                                    <a target="_blank" style="color:#ff6600;font-weight:bold;font-family:helvetica,arial,sans-seif;text-decoration:none" href=" {{$data['forgotpw_link']}} "> {{$data['forgotpw_link']}} </a>.<br>                                                                                    Notes: Until your password has been changed, your current password will remain valid. The Forgot Password Link will be available for a limited time only.

                                                                                    <br><br> {{$data['business_name']}}
                                                                                    <br>
                                                                                    <br>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td width="556" height="30" style="font-size:30px;line-height:30px;height:30px;text-align:left">&nbsp;</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td width="20" height="26" style="font-size:26px;line-height:26px;height:26px;text-align:left"></td>
                                            </tr>
                                            <tr>
                                                <td width="20" height="2" bgcolor="#d9dfe1" style="background-color:#d9dfe1;font-size:2px;line-height:2px;height:2px;text-align:left"></td>
                                                <td width="556" height="2" bgcolor="#d9dfe1" style="background-color:#d9dfe1;font-size:2px;line-height:2px;height:2px;text-align:left"></td>
                                                <td width="20" height="2" bgcolor="#d9dfe1" style="background-color:#d9dfe1;font-size:2px;line-height:2px;height:2px;text-align:left"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td width="37" height="40" style="font-size:40px;line-height:40px;height:40px;text-align:left"></td>
                            </tr>
                            <tr>
                                <td width="38" height="30" style="font-size:30px;line-height:30px;height:30px;text-align:left"></td>
                                <td width="596" height="30" style="font-size:30px;line-height:30px;height:30px;text-align:left"></td>
                                <td width="37" height="30" style="font-size:30px;line-height:30px;height:30px;text-align:left"></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <table cellspacing="0" cellpadding="0" width="100%" border="0" bgcolor="#4b4b4b">
        <tbody>
            <tr>
                <td align="center">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <table cellspacing="0" cellpadding="0" width="100%" border="0" bgcolor="#545454">
        <tbody>
            <tr>
                <td align="center">
                    <table cellspacing="0" cellpadding="0" width="672" border="0" bgcolor="#545454">
                        <tbody>
                            <tr>
                                <td width="38" height="30" style="font-size:30px;line-height:30px;height:30px;text-align:left"></td>
                                <td width="569" height="30" style="font-size:30px;line-height:30px;height:30px;text-align:left"></td>
                                <td width="38" height="30" style="font-size:30px;line-height:30px;height:30px;text-align:left"></td>
                            </tr>
                            <tr>
                                <td width="38" height="40" style="font-size:40px;line-height:40px;text-align:left">
                                </td>
                                <td valign="top" style="font-family:helvetica,arial,sans-seif;font-size:14px;line-height:16px;color:#cccccc;text-align:left">Copyright &copy; {{$data['business_name']}}, All rights reserved.<br><br><br></td>
                                <td width="38" height="40" style="font-size:40px;line-height:40px;text-align:left"></td>
                            </tr>
                            <tr>
                                <td width="38" height="40" style="font-size:40px;line-height:40px;text-align:left"></td>
                                <td width="569" height="40" style="font-size:40px;line-height:40px;text-align:left"></td>
                                <td width="38" height="40" style="font-size:40px;line-height:40px;text-align:left"></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="yj6qo"></div>
    <div class="adL">

    </div>
</div>
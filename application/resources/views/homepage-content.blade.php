@extends('homepage')

@section('content')

    <section class="">
        <div id="myCarousel" class="carousel slide margin-t70" data-interval="false">
          <div class="carousel-inner">
            <div class="item active">
              <img src="<?php echo asset('assets/img/homepage.jpg'); ?>" style="width:100%" class="img-responsive">
            </div>
          </div> 
        </div>


        <div class="brand-list-area">
            <div class="container">
                <p class="text-center">Revolutionizing the application of <span class="themecolor">communications</span> infrastructure within the region </p>
            </div>
        </div>

        <div class="footer-area">
            <div class="footer" style="padding:70px 0;">
                <div class="container">
                    <div class="col-md-8 col-sm-8 footer-one">
                        <h4>Services </h4>
                        <div class="col-md-6 col-sm-6">
                            <ul class="list_check">
                                <li>SMS & IVR</li>
                                <li>Promotional Campaigns</li>
                                <li>Bulk Payments</li>
                            </ul>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <ul class="list_check">
                                <li>Bulk Airtime</li>
                                <li>Bulk SMS</li>
                                <li>Enterprise Web Solutions</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4 footer-two">
                        <h4>Interactive Media Services Ltd</h4>

                        <div class="">
                            <div class="vl"></div>
                            <p class="big">
                            Valley View Business Park, Wing A 7th Floor<br>
                            P.O Box 61823 Nairobi, Kenya,<br>
                            Mobile: [254] [0] 709 084 000, 254] [0] 709 084 200,<br>
                            Email: info@ims.co.ke</p>
                        </div>
                    </div>
                    
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 ">
                            <div class="copyright-text">
                                <p>© <?php echo date('Y');?> Interactive Media Services </p>
                            </div>
                        </div> <!-- End Col -->
                        
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection


{{--External Style Section--}}
@section('style')
    {!! Html::script("assets/libs/chartjs/chart.js")!!}
@endsection
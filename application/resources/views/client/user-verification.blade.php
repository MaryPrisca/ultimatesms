@extends('homepage')

@section('content')

    <section class="" style="padding-top: 100px;">
        <div class="container jumbo-container">
            <div class="row">
                 <div class="row">
                    <div class="col-md-4 col-md-offset-4">

                        <div class="panel panel-30">
                            <div class="panel-heading">
                                <h3 class="panel-title text-center">{{language_data('Verify Your Account')}}</h3>
                            </div>
                            <div class="panel-body">
                                <form class="" role="form" action="{{url('user/post-verification-token')}}" method="post">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" value="{{Auth::guard('client')->user()->id}}" name="cmd">
                                    <button type="submit" class="btn btn-primary btn-block btn-lg">{{language_data('Send Verification Email')}}</button>
                                </form>
                                <br>
                                @include('notification.notify')
                            </div>
                        </div>
                        <div class="panel-other-acction">
                            <div class="text-md text-center">
                                <a href="{{url('login')}}">{{language_data('Back To Sign in')}}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection


{{--External Style Section--}}
@section('style')
    {!! Html::script("assets/libs/chartjs/chart.js")!!}
@endsection
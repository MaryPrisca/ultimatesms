@extends('client')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <!-- <h2 class="page-title">{{language_data('SMS History')}}</h2> -->
            <h2 class="page-title">Payment History</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">



                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{language_data('Search Condition')}}</h3>
                        </div>
                        <div class="panel-body">
                            <form class="" role="form" method="post" id="search-form">

                                <div class="row">

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Transaction Code</label>
                                            <input type="text" class="form-control" name="transaction_code">
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>{{language_data('Status')}}</label>
                                            <input type="text" id="status" class="form-control" name="status">
                                        </div>
                                    </div>


                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>{{language_data('Date')}} {{language_data('From')}}</label>
                                            <input type="text" id="date_from" class="form-control dateTimePicker" name="date_from">
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>{{language_data('Date')}} {{language_data('To')}}</label>
                                            <input type="text" id="date_to" class="form-control dateTimePicker" name="date_to">
                                        </div>
                                    </div>



                                </div>
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <button type="submit" class="btn btn-success pull-right"><i class="fa fa-search"></i> {{language_data('Search')}}</button>

                            </form>
                        </div>
                    </div>
                </div>


                <div class="col-sm-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <button id="deleteTriger" class="btn btn-danger btn-xs pull-right m-r-20"><i class="fa fa-trash"></i> {{language_data('Bulk Delete')}}</button>
                            <!-- <h3 class="panel-title">{{language_data('SMS History')}}</h3> -->
                            <h3 class="panel-title">Payment History</h3>
                        </div>
                        <div class="panel-body">


                            <table class="table data-table table-hover table-ultra-responsive">
                                <thead>
                                <tr>
                                    <th style="width: 5%">

                                        <div class="coder-checkbox">
                                            <input type="checkbox"  id="bulkDelete"  />
                                            <span class="co-check-ui"></span>
                                        </div>

                                    </th>
                                    <th style="width: 30%;">{{language_data('Date')}}</th>
                                    <th style="width: 30%;">Transaction Code</th>
                                    <th style="width: 30%;">Units</th>
                                    <th style="width: 30%;">{{language_data('Amount')}}</th>
                                    <th style="width: 30%;">{{language_data('Status')}}</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}

    <script>
        $(document).ready(function(){

            $("#date_from").on("dp.change", function (e) {
                $('#date_to').data("DateTimePicker").minDate(e.date);
            });

            $("#date_to").on("dp.change", function (e) {
                $('#date_from').data("DateTimePicker").maxDate(e.date);
            });

            var oTable = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                // ajax: '{!! url('get-payment-history-data/') !!}',
                ajax: {
                    url: '{!! url('get-payment-history-data/') !!}',
                    data: function (d) {
                        d.status = $('input[name=status]').val();
                        d.transaction_code = $('input[name=transaction_code]').val();
                        d.date_from = $('input[name=date_from]').val();
                        d.date_to = $('input[name=date_to]').val();
                    }
                },
                columns: [
                    {data: 'id', name: 'id',orderable: false, searchable: false},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'transaction_code', name: 'transaction_code'},
                    {data: 'amount', name: 'amount'},
                    {data: 'units', name: 'units'},
                    {data: 'status', name: 'status'},
                ]
            });

             $('#search-form').on('submit', function(e) {
                oTable.draw();
                e.preventDefault();
              });


            /*For Delete client*/
            // $( "body" ).delegate( ".cdelete", "click",function (e) {
            //     e.preventDefault();
            //     var id = this.id;
            //     bootbox.confirm("Are you sure?", function (result) {
            //         if (result) {
            //             var _url = $("#_url").val();
            //             window.location.href = _url + "/user/delete-user/" + id;
            //         }
            //     });
            // });

        });
    </script>
@endsection
@extends('homepage')

@section('content')

    <section class="" style="padding-top: 90px;">
        <div class="container jumbo-container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                   
                    <div class="panel panel-30">
                        <div class="panel-heading">
                            <!-- <h3 class="panel-title text-center">{{language_data('Sign to your account')}}</h3> -->
                            <h3 class="panel-title text-center"><strong>Account Verification</strong></h3>                        
                        </div>
                        <div class="panel-body">
                            
                            @include('notification.notify')

                            <form class="" role="form" method="post" action="{{url('post-verification-code')}}">
                                <p>In order to avoid misuse of this account an SMS message has been sent to the
                                specified phone number. It contains a verification code that you need to enter below
                                to confirm your account</p>

                                <!-- <div class="form-group">
                                    <label><strong>Verification Code</strong></label>
                                    <input type="text" class="form-control" required name="verification_code" id="v_code" value="{{old('verification_code')}}" placeholder="Insert code">

                                </div> -->

                                <div class="col-md-9">
                                    <div class="form-group">
                                        <label><strong>Verification Code</strong></label>
                                        <input type="text" class="form-control" required name="verification_code" id="v_code" value="{{old('verification_code')}}" placeholder="Insert code">

                                    </div>
                                </div>

                                <div class="text-md text-center" style="float: right; margin-top: 35px;">
                                    <a id="new_code" href="" onclick="send_new_code()">Send new code</a>
                                </div>

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="submit" class="btn btn-primary btn-block btn-lg" value="Validate">
                            </form>
                            <br>

                        </div>
                    </div>
                    <div class="panel-other-acction">
                        <div class="text-md text-center">
                            {{language_data('Already have an Account')}} ? {{language_data('Login')}} <a href="{{url('login')}}">{{language_data('here')}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection


{{--External Style Section--}}
@section('style')
    {!! Html::script("assets/libs/chartjs/chart.js")!!}
@endsection

<script>
    function send_new_code() {
        $.ajax({
            url:'post-new-verification-code',
            type:'post',
            success:function(){
            }
        });
    }
</script>
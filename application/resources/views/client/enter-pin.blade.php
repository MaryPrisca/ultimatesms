@extends('homepage')

@section('content')

    <section class="" style="padding-top: 90px;">
        <div class="container jumbo-container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="panel panel-30">
                        <div class="panel-heading">
                            <!-- <h3 class="panel-title text-center">{{language_data('Sign to your account')}}</h3> -->
                            <h3 class="panel-title text-center" style="font-weight: bold; font-size: 16px;">Enter Pin</h3>                        
                        </div>
                        <div class="panel-body">
                            
                            @include('notification.notify')
                            
                            <form class="" role="form" method="post" action="{{url('post-pin-number')}}">
                                <div class="form-group">
                                    <label>Pin Number</label>
                                    <input type="text" class="form-control" required name="pin_no" value="{{old('pin_no')}}" placeholder="Insert pin number">
                                </div>

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" value="{{Auth::guard('client')->user()->id}}" name="cmd">
                                <!-- <input type="submit" class="btn btn-primary btn-block btn-lg" value="{{language_data('Sign Up')}}"> -->
                                <input type="submit" class="btn btn-primary btn-block btn-lg" value="Submit">
                            </form>
                            <br>

                        </div>
                    </div>
                    <div class="panel-other-acction">
                        <div class="text-md text-center">
                            {{language_data('Already have an Account')}} ? {{language_data('Login')}} <a href="{{url('login')}}">{{language_data('here')}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection


{{--External Style Section--}}
@section('style')
    {!! Html::script("assets/libs/chartjs/chart.js")!!}
@endsection
@extends('homepage')

@section('content')

    <section class="" style="padding-top: 70px;">
        <div class="container jumbo-container"  style="padding-bottom: 10px;">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <!-- <div class="app-logo-inner text-center">
                        <img src="<?php //echo asset(app_config('AppLogo')); ?>" alt="logo" class="bar-logo">
                    </div> -->
                    <div class="panel panel-30">
                        <div class="panel-heading">
                            <h3 class="panel-title text-center" style="font-weight: bold; font-size: 16px;">{{language_data('User Registration')}}</h3>
                        </div>
                        <div class="panel-body">

                            @include('notification.notify')

                            <form class="" role="form" method="post" action="{{url('user/post-registration')}}">

                                <div class="form-group">
                                    <label>{{language_data('First Name')}}</label>
                                    <input type="text" class="form-control" required name="first_name" id="first_name"  value="{{old('first_name')}}">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Last Name')}}</label>
                                    <input type="text" class="form-control" name="last_name" required value="{{old('last_name')}}">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Email')}}</label>
                                    <input type="email" class="form-control" name="email" placeholder="Email address" required value="{{old('email')}}">
                                </div>

                               <!--  <div class="form-group">
                                    <label for="user name">{{language_data('User Name')}}</label>
                                    <input type="text" class="form-control" required name="user_name"  value="{{old('user_name')}}">
                                </div> -->

                                <div class="form-group">
                                    <label>{{language_data('Password')}}</label>
                                    <input type="password" class="form-control" required name="password"  value="{{old('password')}}">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Confirm Password')}}</label>
                                    <input type="password" class="form-control" required name="cpassword"  value="{{old('cpassword')}}">
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Phone')}}</label>
                                    <input type="text" class="form-control" required name="phone" placeholder="e.g. 254700000000"  value="{{old('phone')}}">  <!-- rel="txtTooltip" title="Use your current phone number" data-toggle="tooltip" data-placement="top" -->
                                </div>

                                <div class="form-group">
                                    <label>{{language_data('Company')}}</label>
                                    <input type="text" class="form-control" required name="company"  value="{{old('company')}}">
                                </div>

                                <div class="form-group">
                                    <label for="Country">{{language_data('Country')}}</label>
                                    <select name="country" class="form-control selectpicker" data-live-search="true"  value="{{old('country')}}">
                                        {!!countries(app_config('Country'))!!}
                                    </select>
                                </div>


                            @if(app_config('captcha_in_client_registration')=='1')
                                    <div id="g-recaptcha" class="g-recaptcha" data-sitekey="{{app_config('captcha_site_key')}}" data-expired-callback="recaptchaCallback"></div>

                                    <noscript>
                                        <div style="width: 302px; height: 352px;margin-bottom:20px;margin-left:100px;">
                                            <div style="width: 302px; height: 352px; position: relative;">
                                                <div style="width: 302px; height: 352px; position: absolute;">
                                                    <!-- change YOUR_SITE_KEY with your google recaptcha key -->
                                                    <iframe src="https://www.google.com/recaptcha/api/fallback?k={{app_config('captcha_site_key')}}" style="width: 302px; height:352px; border-style: none;">
                                                    </iframe>
                                                </div>
                                                <div style="width: 250px; height: 80px; position: absolute; border-style: none; bottom: 21px; left: 25px; margin: 0px; padding: 0px; right: 25px;">
                                                    <textarea id="g-recaptcha-response" name="g-recaptcha-response" class="g-recaptcha-response" style="width: 250px; height: 80px; border: 1px solid #c1c1c1; margin: 0px; padding: 0px; resize: none;"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </noscript>
                                @endif


                                <div class="form-group m-t-20 m-b-20">
                                    <div class="form-group">
                                        <div class="coder-checkbox">
                                            <input type="checkbox" checked="" value="yes" name="email_notify">
                                            <span class="co-check-ui"></span>
                                            <!-- <label>{{language_data('Notify Client with email')}}</label> -->
                                            <label>Share offers and cool updates with me</label>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="submit" class="btn btn-primary btn-block btn-lg" value="{{language_data('Sign up')}}">
                            </form>
                            <br>

                        </div>
                    </div>
                    <div class="panel-other-acction">
                        <div class="text-md text-center">

                            <a href="{{url('forgot-password')}}">{{language_data('Forget Password')}}?</a><br>
                            {{language_data('Already have an Account')}} ? {{language_data('Login')}} <a href="{{url('login')}}">{{language_data('here')}}</a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection


{{--External Style Section--}}
@section('style')
    {!! Html::script("assets/libs/chartjs/chart.js")!!}
@endsection
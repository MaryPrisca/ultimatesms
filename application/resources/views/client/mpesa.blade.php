@extends('client')

@section('content')

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <!-- <h2 class="page-title">{{language_data('Pay with Credit Card')}}</h2> -->
            <h2 class="page-title">Pay Using MPesa</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            <div class="row">
                <div class="col-lg-6">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Pay Using MPesa</h3>
                           
                            <br>
                            <div class="alert alert-success" role="alert">
                                Payment request made, <strong>check your phone</strong> to complete payment.
                            </div>
                            <!-- <p><span class="lead"><strong>Disclaimer: </strong></span><span class="text-warning">Your sim card needs to be upgraded to complete this payment.</span></p> -->
                                

                            <button class="btn pull-right" onclick="retry()" style="background-color: #00a84f !important; border: none;" onMouseOver="this.style.opacity='0.8'" onMouseOut="this.style.opacity='1'">Retry</button>
                        </div>
                        <div class="panel-body">
                            
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>

@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
@endsection

<script>
    function retry() {
        location.reload();
    }
</script>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{app_config('AppTitle')}}</title>
    <link rel="icon" type="image/x-icon"  href="<?php echo asset(app_config('AppFav')); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    {{--Global StyleSheet Start--}}
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
    {!! Html::style("assets/libs/bootstrap/css/bootstrap.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-toggle/css/bootstrap-toggle.min.css") !!}
    {!! Html::style("assets/libs/font-awesome/css/font-awesome.min.css") !!}
    {!! Html::style("assets/libs/alertify/css/alertify.css") !!}
    {!! Html::style("assets/libs/alertify/css/alertify-bootstrap-3.css") !!}
    {!! Html::style("assets/libs/bootstrap-select/css/bootstrap-select.min.css") !!}

    {{--Custom StyleSheet Start--}}

    @yield('style')

    {{--Global StyleSheet End--}}

    {!! Html::style("assets/css/style.css") !!}
    {!! Html::style("assets/css/homepage.css") !!}
    {{--!! Html::style("assets/css/admin.css") !!--}}
    {!! Html::style("assets/css/responsive.css") !!}


</head>



<body class="homepage">

    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- <a class="navbar-brand" href="#">Gaadiexpert</a> -->
                <a class="navbar-brand" href="{{url('/')}}"><img src="<?php echo asset('assets/img/IMS_Logo_Small.png'); ?>"  width="160px" height="50px" class="img-responsive"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">


                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="#">SERVICES</a></li>
                    <li><a href="#">ABOUT US</a></li>
                    <li><a href="#">CONTACT US</a></li> 

                    @if(Auth::guard('client')->user())
                        <li class="dropdown user-profile-home">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="user-info text-uppercase" style="color: #6a6c74;">{{Auth::guard('client')->user()->fname}} {{Auth::guard('client')->user()->lname}}</span> <b class="caret"></b>
                                @if(Auth::guard('client')->user()->image!='')
                                    <img class="user-image" src="<?php echo asset('assets/client_pic/'.Auth::guard('client')->user()->image); ?>" alt="{{Auth::guard('client')->user()->fname}} {{Auth::guard('client')->user()->lname}}">
                                @else
                                    <img class="user-image" src="<?php echo asset('assets/client_pic/profile.jpg'); ?>" alt="{{Auth::guard('client')->user()->fname}} {{Auth::guard('client')->user()->lname}}">
                                @endif
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="{{url('dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                                <li><a href="{{url('user/edit-profile')}}"><i class="fa fa-edit"></i> {{language_data('Update Profile')}}</a></li>
                                    <li><a href="{{url('user/change-password')}}"><i class="fa fa-lock"></i> {{language_data('Change Password')}}</a></li>
                                    <li class="bg-dark">
                                        <a href="{{url('logout')}}" class="clearfix">
                                            <span class="pull-left">{{language_data('Logout')}}</span>
                                            <span class="pull-right"><i class="fa fa-power-off"></i></span>
                                        </a>
                                    </li>
                            </ul>
                        </li>
                    @else
                        <li><a class="btn btn-primary btn-sm" href="{{url('login')}}" role="button" style="margin-right: 5px;">Sign In</a></li>          
                        <li><a class="btn btn-primary btn-sm" href="{{url('signup')}}" role="button" style="margin-right: 5px;">Create Account</a></li>
                    @endif  

                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-collapse -->
    </nav>
    



    {{--Content File Start Here--}}

    @yield('content')

    {{--Content File End Here--}}

    



{{--Global JavaScript Start--}}
{!! Html::script("assets/libs/jquery-1.10.2.min.js") !!}
{!! Html::script("assets/libs/jquery.slimscroll.min.js") !!}
{!! Html::script("assets/libs/bootstrap/js/bootstrap.min.js") !!}
{!! Html::script("assets/libs/bootstrap-toggle/js/bootstrap-toggle.min.js") !!}
{!! Html::script("assets/libs/alertify/js/alertify.js") !!}
{!! Html::script("assets/libs/bootstrap-select/js/bootstrap-select.min.js") !!}
{!! Html::script("assets/js/scripts.js") !!}
{{--Global JavaScript End--}}

<script type="text/javascript">
    $(function(){
        $(".dropdown").hover(            
          function() {
            $('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
            $(this).toggleClass('open');
            $('b', this).toggleClass("caret caret-up");                
          },
          function() {
            $('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
            $(this).toggleClass('open');
            $('b', this).toggleClass("caret caret-up");                
          });
      });
</script>

</body>

</html>
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
    	'client_id',
		'merchant_request_id',
		'checkout_request_id',
        'reference',
		'transaction_code',
        'amount',
		'units',
		'status',
    ];

    // protected $with = [
    //     'client'
    // ];

    public function client(){
        return $this->belongsTo(Client::class);
    }
}

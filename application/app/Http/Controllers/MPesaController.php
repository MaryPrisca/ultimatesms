<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;

use App\Payment;
use App\Client;

class MPesaController extends Controller
{
    public function stkCallback(Request $request)
    {
    	$payment = Payment::where('merchant_request_id', $request->Body['stkCallback']['MerchantRequestID'])
    			->where('checkout_request_id', $request->Body['stkCallback']['CheckoutRequestID'])
    			->where('status', 'PENDING')
    			->firstOrFail();

    	if ($request->Body['stkCallback']['ResultCode'] == 0) {
    		$payment->update([
    			'status' => 'COMPLETED', 'transaction_code' => $request->Body['stkCallback']['CallbackMetadata']['Item'][1]['Value']
    		]);

    		$client = Client::findOrFail($payment->client_id);

    		$client->update([
    			'sms_limit' => $client->sms_limit += $payment->units
    		]);
    	} else {
    		$payment->update([
    			'status' => 'FAILED'
    		]);
    	}
    }

    public function buyUnitWithMpesa()
    {
    	# code...
    }

    public function paymentHistory()
    {
        return view('client.payment-history');
    }

    public function getPaymentHistoryData(Request $request)
    {
        if ($request->has('order') && $request->has('columns')) {
            $order_col_num = $request->get('order')[0]['column'];
            $get_search_column = $request->get('columns')[$order_col_num]['name'];
            $short_by = $request->get('order')[0]['dir'];

            if ($get_search_column == 'date'){
                $get_search_column = 'updated_at';
            }

        } else {
            $get_search_column = 'updated_at';
            $short_by = 'DESC';
        }

        $payment_history = Payment::select(['id', 'client_id', 'merchant_request_id', 'checkout_request_id', 'reference', 'transaction_code', 'amount', 'units', 'status', 'created_at', 'updated_at'])->where('client_id', Auth::guard('client')->user()->id)->orderBy($get_search_column, $short_by);

        return Datatables::of($payment_history)
            ->addColumn('created_at', function ($cl) {
                $t = strtotime($cl->created_at);
                return date('jS M o H:i:s', $t);
            })
            ->addColumn('status', function ($cl) {
                if ($cl->status == 'COMPLETED') {
                    return '<span class="label label-success">COMPLETED</span>';
                } elseif ($cl->status == 'FAILED') {
                    return '<span class="label label-danger">FAILED</span>';
                } else {
                    return '<span class="label label-warning">PENDING</span>';
                }
            })
            ->addColumn('transaction_code', function ($cl) {
                if (!$cl->transaction_code) {
                    return "NULL";
                } else {
                    return $cl->transaction_code;
                }
            })
            ->filter(function ($query) use ($request) {

                if ($request->has('transaction_code')) {
                    $query->where('transaction_code', 'like', "%{$request->get('transaction_code')}%");
                }

                if ($request->has('status')) {
                    $query->where('status', 'like', "%{$request->get('status')}%");
                }

                if ($request->has('date_from') && $request->has('date_to')) {
                    $date_from = date('Y-m-d H:i:s', strtotime($request->get('date_from')));
                    $date_to = date('Y-m-d H:i:s', strtotime($request->get('date_to')));
                    $query->whereBetween('created_at', [$date_from, $date_to]);
                }
            })
            ->escapeColumns([])
            ->make(true);
    }

    public function paymentHistoryAdmin()
    {
        return view('admin.all-payments');
    }

    public function getAdminPaymentHistoryData(Request $request)
    {
        if ($request->has('order') && $request->has('columns')) {
            $order_col_num = $request->get('order')[0]['column'];
            $get_search_column = $request->get('columns')[$order_col_num]['name'];
            $short_by = $request->get('order')[0]['dir'];

            if ($get_search_column == 'date'){
                $get_search_column = 'updated_at';
            }

        } else {
            $get_search_column = 'updated_at';
            $short_by = 'DESC';
        }

        // $payment = Payment::with('client')->get();
        $payment = Payment::select(['id', 'client_id', 'merchant_request_id', 'checkout_request_id', 'reference', 'transaction_code', 'amount', 'units', 'status', 'created_at', 'updated_at'])->orderBy($get_search_column, $short_by)->with('client');


        // $payment = Payment::all();

        // $payment->load(['client' => function ($query) {
        //     $query->orderBy('fname', 'asc');
        // }]);




        return Datatables::of($payment)
            ->addColumn('created_at', function ($cl) {
                $t = strtotime($cl->created_at);
                return date('jS M o H:i:s', $t);
            })
            ->addColumn('name', function ($cl) {
                return $cl->client->fname . ' ' . $cl->client->lname ;
            })
            ->addColumn('status', function ($cl) {
                if ($cl->status == 'COMPLETED') {
                    return '<span class="label label-success">COMPLETED</span>';
                } elseif ($cl->status == 'PENDING') {
                    return '<span class="label label-warning">PENDING</span>';
                } else {
                    return '<span class="label label-danger">FAILED</span>';
                }
            })
            ->addColumn('transaction_code', function ($cl) {
                if (!$cl->transaction_code) {
                    return "NULL";
                } else {
                    return $cl->transaction_code;
                }
            })
            ->filter(function ($query) use ($request) {

                if ($request->has('transaction_code')) {
                    $query->where('transaction_code', 'like', "%{$request->get('transaction_code')}%");
                }

                if ($request->has('status')) {
                    $query->where('status', 'like', "%{$request->get('status')}%");
                }

                if ($request->has('date_from') && $request->has('date_to')) {
                    $date_from = date('Y-m-d H:i:s', strtotime($request->get('date_from')));
                    $date_to = date('Y-m-d H:i:s', strtotime($request->get('date_to')));
                    $query->whereBetween('updated_at', [$date_from, $date_to]);
                }
            })
            ->escapeColumns([])
            ->make(true);
    }
}

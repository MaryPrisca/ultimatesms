<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;

use App\SenderIdManage;

class RedirectIfNoSenderID
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {     
        $all_sender_id = SenderIdManage::where('status', 'unblock')->get();
        $all_ids = [];

        foreach ($all_sender_id as $sid) {
            $client_array = json_decode($sid->cl_id);

            if (is_array($client_array) && in_array('0', $client_array)) {
                array_push($all_ids, $sid->id);
            } elseif (is_array($client_array) && in_array(Auth::guard('client')->user()->id, $client_array)) {
                array_push($all_ids, $sid->id);
            }
        }
        $sender_ids = array_unique($all_ids);

        $sender_ids = SenderIdManage::whereIn('id', $sender_ids)->first();

        if (!$sender_ids) {
            return redirect('user/sms/sender-id-management')->with([
                'message' => 'Request for a sender ID to proceed. If you\'ve already done so, wait for a response in 24 Hrs.',
                'message_important' => true
            ]);
        }

        return $next($request);
    }
}

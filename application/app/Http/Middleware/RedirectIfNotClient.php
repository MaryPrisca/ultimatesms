<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfNotClient
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'client')
    {
        if (!Auth::guard($guard)->check()){
            return redirect('login')->with([
                'message'=> language_data('Invalid Access'),
                'message_important'=>true
            ]);
        }
        elseif(Auth::guard($guard)->user()->verified=='0'){
            return redirect('signup-verification');
        }
        elseif(Auth::guard($guard)->user()->status=='Closed'){
            Auth::guard($guard)->logout();
            
            return redirect('/login')->withErrors([
                'email' => "Account is closed. Please contact the administrator."
            ]);
        }
        
        return $next($request);
    }
}

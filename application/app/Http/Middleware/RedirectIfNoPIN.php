<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;

class RedirectIfNoPIN
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // dd($this->auth->user());

        // if (!Auth::guard($guard)->check()){
        //     return redirect('login')->with([
        //         'message'=> language_data('Invalid Access'),
        //         'message_important'=>true
        //     ]);
        // }
        // elseif(Auth::guard($guard)->user()->status=='Inactive'){
        //     return redirect('enter-pin');
        // }
        
        if (!Auth::guard('client')->user()->pin_number) {
            return redirect('enter-pin');
        }

        return $next($request);
    }
}
